/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package chat;

import java.util.Iterator;

import mfw.buff.Returnable;
import mfw.ctrl.Trigger;
import mfw.ctrl.Cmd.Parameter;
import mfw.fw.Client;

public class ShowClients extends Trigger {

	protected void execute(Returnable subject, Parameter param) {
		if (((Client) subject).attachment() == null) {
			byte[] error = "Please login!\r\n".getBytes();
			subject.returnResult(error, 0, error.length);
			return;
		}
		Iterator<Client> iter = Telnet_ChatRoom.list.iterator();
		while (iter.hasNext()) {
			byte[] client = (iter.next().attachment().toString() + "\r\n")
					.getBytes();
			subject.returnResult(client, 0, client.length);
		}
	}
}
