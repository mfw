/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.parsers.DocumentBuilderFactory;
import mfw.buff.CommandReceiver;
import mfw.buff.Returnable;
import mfw.ctrl.ReceiverFactory;
import mfw.ctrl.Trigger;
import mfw.ctrl.Cmd.Parameter;
import mfw.fw.Client;
import mfw.fw.Server;
import mfw.pool.Mission;
import mfw.timer.ClockThread;
import mfw.timer.TimerEvent;
import mfw.timer.TimerEventGroup;

public class Telnet_ChatRoom {
	private static boolean flag = true;
	private static Server srv = new Server();
	private static CommandReceiver chatReceiver;
	private static CommandReceiver adminReceiver;
	static {
		try {
			chatReceiver = ReceiverFactory
					.newReceiverInstance(DocumentBuilderFactory
							.newInstance()
							.newDocumentBuilder()
							.parse(
									ClassLoader
											.getSystemResourceAsStream("chat/chat_cmd.xml"))
							.getDocumentElement());
			adminReceiver = ReceiverFactory
					.newReceiverInstance(DocumentBuilderFactory
							.newInstance()
							.newDocumentBuilder()
							.parse(
									ClassLoader
											.getSystemResourceAsStream("chat/admin_cmd.xml"))
							.getDocumentElement());
		} catch (Exception e) {
			throw new Error(e);
		}
	}

	private static Returnable admin = new Returnable() {

		public boolean isReturnable() {
			return true;
		}

		public void returnResult(byte[] result, int offset, int len) {
			System.out.print(new String(result, offset, len));
		}
	};

	public static Set<Client> list = new HashSet<Client>();

	private static TimerEventGroup timer = new TimerEventGroup();

	private static byte[] content = new byte[0];

	public static void main(String[] args) throws IOException {
		Mission.begin(256);
		new ClockThread(timer, 30000).start();
		timer.add(new CleanTimer());
		timer.add(new PubTimer());
		srv.setWelcomeStr("Welcome!\r\n".getBytes());
		srv.setAddress(new InetSocketAddress(8081));
		srv.setCmdLength(256);
		srv.setCoreReceiver(chatReceiver);
		srv.start();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (flag) {
			byte[] cmd = in.readLine().getBytes();
			adminReceiver.receive(admin, cmd, 0, cmd.length);
		}
		srv.stop();
		timer.cancel();
		Mission.end();
	}

	public static class ListSockets extends Trigger {

		protected void execute(Returnable subject, Parameter param) {
			Iterator<SelectionKey> iter = srv.getKeys().iterator();
			while (iter.hasNext()) {
				byte[] socket = (iter.next().channel().toString() + "\r\n")
						.getBytes();
				subject.returnResult(socket, 0, socket.length);
			}
		}
	}

	public static class ListClients extends Trigger {

		protected void execute(Returnable subject, Parameter param) {
			Iterator<Client> iter = list.iterator();
			while (iter.hasNext()) {
				byte[] client = (iter.next().attachment().toString() + "\r\n")
						.getBytes();
				subject.returnResult(client, 0, client.length);
			}
		}
	}

	public static class StopClient extends Trigger {

		protected void execute(Returnable subject, Parameter param) {
			byte[] warning = "Warning!!!\r\n".getBytes();
			String nick = new String(param.getBytes());
			Iterator<Client> iter = list.iterator();
			while (iter.hasNext()) {
				Client cl = iter.next();
				if (cl.attachment().toString().equals(nick)) {
					cl.returnResult(warning, 0, warning.length);
					cl.attach(null);
					iter.remove();
				}
			}
		}
	}

	public static class Exit extends Trigger {

		protected void execute(Returnable subject, Parameter param) {
			byte[] bye = "GoodBye!\r\n".getBytes();
			subject.returnResult(bye, 0, bye.length);
			flag = false;
		}
	}

	public static class Pub extends Trigger {

		protected void execute(Returnable subject, Parameter param) {
			content = param.getBytes();
		}
	}

	static class PubTimer extends TimerEvent {

		protected boolean isContinue() {
			Iterator<Client> iter = list.iterator();
			while (iter.hasNext()) {
				iter.next().returnResult(content, 0, content.length);
			}
			return true;
		}
	}

	static class CleanTimer extends TimerEvent {

		protected boolean isContinue() {
			Iterator<Client> iter = list.iterator();
			while (iter.hasNext()) {
				if (!iter.next().isReturnable())
					iter.remove();
			}
			return true;
		}
	}
}