/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package chat;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import mfw.buff.Returnable;
import mfw.ctrl.Trigger;
import mfw.ctrl.Cmd.Parameter;
import mfw.fw.Client;

public class Login extends Trigger {

	protected void execute(Returnable subject, Parameter param) {
		String uname = new String(param.getVerb().getBytes());
		String upwd = new String(param.getParameter().getBytes());
		try {
			if (upwd
					.equals(DocumentBuilderFactory
							.newInstance()
							.newDocumentBuilder()
							.parse(
									ClassLoader
											.getSystemResourceAsStream("chat/userinfo.xml"))
							.getElementsByTagName(uname).item(0)
							.getAttributes().getNamedItem("password")
							.getNodeValue())) {
				((Client) subject).attach(uname);
				Telnet_ChatRoom.list.add((Client) subject);
			}
		} catch (NullPointerException e) {
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

}
