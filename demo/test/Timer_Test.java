/**
 *    Copyright 2011 Huize Dai <geb.1989@gmail.com>
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package test;

import mfw.pool.Mission;
import mfw.timer.newer.*;

public class Timer_Test {
	public static ClockThreadGroup clock = new ClockThreadGroup(3, true);
	public static final Object lock = new Object();

	public static void main(String[] args) throws Throwable {
		for (int i = 1; i < 10; i++) {
			new MissionImpl(i).start();
		}
		Thread.sleep(1000);
		synchronized (lock) {
			lock.notifyAll();
		}
		Thread.sleep(5000);
		clock.close();
	}

	static class MissionImpl extends Mission {
		private int id;

		public MissionImpl(int id) {
			this.id = id;
		}

		public void run() {
			synchronized (lock) {
				try {
					lock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
					return;
				}
			}
			for (int i = 1; i < 10; i++) {
				clock.add(new TimerEventImpl(id, i), 10 * i);
			}
		}
	}

	static class TimerEventImpl extends TimerEvent {

		private int id;
		private int num;
		private int time = 0;
		private long previous = System.currentTimeMillis();
		private int[] log = new int[5];

		public TimerEventImpl(int id, int num) {
			this.id = id;
			this.num = num;
		}

		public boolean isContinue() {
			long now;
			System.out.println(now = System.currentTimeMillis());
			log[time++] = (int) (now - previous);
			previous = now;
			if (time >= 5) {
				System.out.println("-------" + id + " " + num + " " + time
						+ ":" + log[0] + "," + log[1] + "," + log[2] + ","
						+ log[3] + "," + log[4]);
			}
			return time < 5;
		}
	}
}
