/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.sel;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;

public final class CoreSelector {

	private Selector core;
	private boolean isContinue;

	public CoreSelector(SelectorInitTool sit) {
		try {
			sit.init(core = Selector.open());
		} catch (IOException e) {
			throw new Error(e);
		}
		isContinue = true;
		new Thread() {

			public void run() {
				try {
					while (isContinue) {
						if (core.select() > 0) {
							for (Iterator<SelectionKey> iter = core
									.selectedKeys().iterator(); iter.hasNext();) {
								SelectionKey key = iter.next();
								((KeyProcesser) key.attachment()).process(key);
								iter.remove();
							}
						}
					}
				} catch (IOException ex) {
					throw new Error(ex);
				} finally {
					for (Iterator<SelectionKey> iter = core.keys().iterator(); iter
							.hasNext();) {
						SelectionKey key = iter.next();
						try {
							key.channel().close();
						} catch (IOException ex) {
						}
					}
					try {
						core.close();
					} catch (IOException ex) {
					}
				}
			}
		}.start();
	}

	public void close() {
		if (!core.isOpen()) {
			return;
		}
		if (isContinue) {
			isContinue = false;
			core.wakeup();
		}
	}
}
