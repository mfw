/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.buff;

public class ContentBuffer {

	private final byte[] buff;
	private int ptr = 0;
	private final CommandReceiver receiver;
	private final Returnable subject;

	public ContentBuffer(Returnable subject, CommandReceiver receiver, int len) {
		this.subject = subject;
		this.receiver = receiver;
		buff = new byte[len];
	}

	public void put(byte[] cmd, int offset, int len) {
		if (receiver != null) {
			receiver.receive(subject, cmd, offset, len);
		}
	}

	public void put(byte ch) {
		if (ch == '\r' || ch == '\n' || ptr >= buff.length) {
			this.put(buff, 0, ptr);
			ptr = 0;
			if (ch != '\r' && ch != '\n') {
				this.put(ch);
			}
		} else if (ch == '\b') {
			if (ptr > 0) {
				ptr--;
			}
		} else if (ch != ' ' || ptr > 0) {
			buff[ptr++] = ch;
		}
	}
}