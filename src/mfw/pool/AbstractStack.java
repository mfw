/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.pool;

public class AbstractStack<Type extends Object> {

	private final Object[] list;
	private int ptr = 0;

	public AbstractStack(int len) {
		list = new Object[len];
	}

	public synchronized boolean push(Type core) {
		if (ptr < list.length) {
			list[ptr++] = core;
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public synchronized Type pop() {
		if (ptr > 0) {
			return (Type) list[--ptr];
		} else {
			return null;
		}
	}
}
