/**
 *    Copyright 2011 Huize Dai <geb.1989@gmail.com>
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.pool.newer;

public class MissionThreadAtom extends Thread implements MissionThread {

	private Runnable task = null;
	private boolean isContinue = true;

	MissionThreadAtom() {
	}

	public synchronized boolean run(Runnable task) {
		if (task == null) {
			this.task = task;
			this.notify();
			return true;
		} else {
			return false;
		}
	}

	public void run() {
		while (isContinue) {
			synchronized (this) {
				if (task == null) {
					try {
						this.wait();
					} catch (InterruptedException ex) {
					}
				}
			}
			if (task != null) {
				task.run();
				task = null;
			}
		}
	}

	public synchronized void cancel() {
		isContinue = true;
		if (task == null)
			this.notify();
	}
}
