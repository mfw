/**
 *    Copyright 2011 Huize Dai <geb.1989@gmail.com>
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.pool.newer;

public class MissionThreadGroup implements MissionThread {

	private final MissionThread[] list;

	MissionThreadGroup(int len, MissionThreadCreator creator) {
		this.list = new MissionThread[len];
		for (int i = 0; i < list.length; i++)
			list[i] = creator.createMissionThread();
	}

	public boolean run(Runnable mission) {
		for (int i = 0; i < list.length; i++)
			if (list[i].run(mission))
				return true;
		return false;
	}

	public void cancel() {
		for (int i = 0; i < list.length; i++)
			list[i].cancel();
	}
}
