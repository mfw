/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.pool;

public class Mission implements Runnable {

	private static final AbstractStack<MissionCore> NULL_STACK = new AbstractStack<MissionCore>(
			0);
	private static AbstractStack<MissionCore> pool = NULL_STACK;

	public synchronized static void begin(int length) {
		if (pool == NULL_STACK) {
			pool = new AbstractStack<MissionCore>(length);
		}
	}

	public synchronized static void end() {
		AbstractStack<MissionCore> tPool = pool;
		pool = NULL_STACK;
		Runnable nullMission = new Runnable() {

			public void run() {
			}
		};
		MissionCore current;
		while ((current = tPool.pop()) != null) {
			current.setMission(nullMission);
		}
	}

	private Runnable task = null;

	public Mission() {
	}

	public Mission(Runnable task) {
		this.task = task;
	}

	public void setMission(Runnable task) {
		this.task = task;
	}

	public void start() {
		MissionCore core = pool.pop();
		if (core == null) {
			core = new MissionCore();
		}
		core.setMission(this);
	}

	public void run() {
		if (task != null) {
			task.run();
		}
	}

	private static class MissionCore implements Runnable {

		private Runnable task = null;

		{
			new Thread(this).start();
		}

		public synchronized void setMission(Runnable task) {
			this.task = task;
			this.notify();
		}

		public void run() {
			while (true) {
				synchronized (this) {
					if (task == null) {
						try {
							this.wait();
						} catch (InterruptedException ex) {
						}
					}
				}
				if (task != null) {
					task.run();
					task = null;
					if (!pool.push(this)) {
						return;
					}
				}
			}
		}
	}
}
