/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.timer;

public class TimerEventGroup extends TimerEvent {

	private boolean isContinue = true;
	private final Object lock = new Object();

	public void add(TimerEvent event) {
		synchronized (lock) {
			event.next = this.next;
			this.next = event;
		}
	}

	public void cancel() {
		isContinue = false;
	}

	protected boolean isContinue() {
		synchronized (lock) {
			while (this.next != null && !this.next.isContinue())
				this.next = this.next.next;
			if (this.next == null)
				return isContinue;
		}
		TimerEvent ptr = this.next;
		while (ptr.next != null) {
			if (!ptr.next.isContinue()) {
				ptr.next = ptr.next.next;
			} else {
				ptr = ptr.next;
			}
		}
		return isContinue;
	}
}
