/**
 *    Copyright 2011 Huize Dai <geb.1989@gmail.com>
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.timer.newer;

public class ClockThread extends Thread {
	private final TimerEvent head;

	public ClockThread() {
		this.head = new TimerEvent() {

			public boolean isContinue() {
				return true;
			}
		};
	}

	ClockThread(TimerEvent head) {
		this.head = head;
	}

	private final Object lock = new Object();
	private TimerEvent current;
	private boolean flag = true;

	public void add(TimerEvent event, long diff) {
		event.diff = diff;
		event.nextTime = System.currentTimeMillis() + diff;
		try {
			boolean needPut = false;
			synchronized (lock) {
				if (!_test(event)) {
					head.lock();
					needPut = true;
				}
			}
			if (needPut)
				_put(event);
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}

	public void close() {
		synchronized (lock) {
			flag = false;
			lock.notify();
		}
	}

	private void _put(TimerEvent event) throws InterruptedException {
		TimerEvent ptr, swap;
		(ptr = head).lock();
		while (true) {
			if (ptr.next != null && ptr.next.nextTime < event.nextTime) {
				(swap = ptr.next).lock();
				ptr.unlock();
				ptr = swap;
				continue;
			}
			event.next = ptr.next;
			ptr.next = event;
			break;
		}
		ptr.unlock();
	}

	private TimerEvent _get() throws InterruptedException {
		head.lock();
		TimerEvent swap = head.next;
		if (swap != null) {
			swap.lock();
			head.next = swap.next;
			swap.unlock();
		}
		head.unlock();
		return swap;
	}

	public void run() {
		long now;
		try {
			synchronized (lock) {
				while (flag) {
					if (current == null && (current = _get()) == null) {
						lock.wait();
					} else if ((now = System.currentTimeMillis()) < current.nextTime) {
						lock.wait(current.nextTime - now);
					} else {
						if (current.isContinue()) {
							current.nextTime += current.diff;
							_put(current);
						}
						current = null;
					}
				}
			}
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}

	private boolean _test(TimerEvent event) throws InterruptedException {
		synchronized (lock) {
			if (current == null) {
				current = event;
				lock.notify();
				return true;
			} else if (current.nextTime > event.nextTime) {
				_put(current);
				current = event;
				lock.notify();
				return true;
			} else {
				return false;
			}
		}
	}

	static class Lock {
		private Thread keyThread = null;

		public synchronized void lock() throws InterruptedException {
			Thread current = Thread.currentThread();
			while (keyThread != current) {
				if (keyThread == null)
					keyThread = current;
				else
					this.wait();
			}
		}

		public synchronized void unlock() {
			if (keyThread == Thread.currentThread()) {
				keyThread = null;
				this.notify();
			}
		}
	}
}
