/**
 *    Copyright 2011 Huize Dai <geb.1989@gmail.com>
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.timer.newer;

public class EventGroup extends TimerEvent implements Event {

	private final Event[] atoms;
	private int ptr = 0;
	private boolean isContinue = true;
	private final Object lock = new Object();

	public EventGroup(int len) {
		atoms = new Event[len];
	}

	public boolean add(Event event) {
		synchronized (lock) {
			if (ptr < atoms.length) {
				atoms[ptr++] = event;
				return true;
			} else {
				return false;
			}
		}
	}

	public void close() {
		isContinue = false;
	}

	public boolean isContinue() {
		for (int i = 0; i < ptr;) {
			if (!atoms[i].isContinue()) {
				synchronized (lock) {
					atoms[i] = atoms[--ptr];
				}
			} else {
				i++;
			}
		}
		return isContinue;
	}

}
