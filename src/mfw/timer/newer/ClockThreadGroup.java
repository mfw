/**
 *    Copyright 2011 Huize Dai <geb.1989@gmail.com>
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.timer.newer;

public class ClockThreadGroup {

	private ClockThread[] clockPool;
	private int i = 0;

	public ClockThreadGroup(int count, boolean isSeries) {
		clockPool = new ClockThread[count];
		if (isSeries) {
			TimerEvent event = new TimerEvent() {

				public boolean isContinue() {
					return true;
				}
			};
			for (int i = 0; i < clockPool.length; i++) {
				(clockPool[i] = new ClockThread(event)).start();
			}
		} else {
			for (int i = 0; i < clockPool.length; i++) {
				(clockPool[i] = new ClockThread()).start();
			}
		}
	}

	public void add(TimerEvent event, long diff) {
		ClockThread clock;
		synchronized (this) {
			clock = clockPool[i = (i + 1) % clockPool.length];
		}
		clock.add(event, diff);
	}

	public void close() {
		for (int i = 0; i < clockPool.length; i++) {
			clockPool[i].close();
		}
	}
}
