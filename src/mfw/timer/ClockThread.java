/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.timer;

public class ClockThread extends Thread {
	private final TimerEvent event;
	private final long t;

	public ClockThread(TimerEvent event, long t) {
		this.event = event;
		this.t = t;
	}

	public void run() {
		long previous = System.currentTimeMillis();
		long diff;
		while (event.isContinue()) {
			diff = System.currentTimeMillis() - previous;
			while (diff < t) {
				try {
					Thread.sleep(t - diff);
				} catch (InterruptedException ex) {
				}
				diff = System.currentTimeMillis() - previous;
			}
			previous += t;
		}
	}
}
