/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.fw;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

import mfw.buff.ContentBuffer;
import mfw.pool.Mission;
import mfw.sel.KeyProcesser;

public class ClientProcesser implements KeyProcesser {

	public static final int BUFFER_LENGTH = 64;
	private ByteBuffer inPipe = ByteBuffer.allocateDirect(BUFFER_LENGTH);
	private ByteBuffer outPipe = ByteBuffer.allocateDirect(BUFFER_LENGTH);

	{
		this.inPipe.clear();
		this.outPipe.clear();
	}
	private final Mission pushMission = new Mission() {

		public void run() {
			outPipe.flip();
			while (outPipe.hasRemaining()) {
				buffer.put(outPipe.get());
			}
			isStarting = false;
		}
	};
	private boolean isStarting = false;
	private final ContentBuffer buffer;

	public ClientProcesser(ContentBuffer buffer) {
		this.buffer = buffer;
	}

	public void process(SelectionKey key) {
		int len = -1;
		try {
			len = ((SocketChannel) key.channel()).read(inPipe);
		} catch (IOException ex) {
		}
		if (len < 0) {
			key.cancel();
			try {
				key.channel().close();
			} catch (IOException ex) {
			}
		} else if (!isStarting) {
			ByteBuffer tempPipe = outPipe;
			outPipe = inPipe;
			isStarting = true;
			pushMission.start();
			this.inPipe = tempPipe;
			this.inPipe.clear();
		}
	}
}
