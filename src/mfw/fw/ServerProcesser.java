/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.fw;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import mfw.buff.CommandReceiver;
import mfw.buff.ContentBuffer;
import mfw.sel.KeyProcesser;

public class ServerProcesser implements KeyProcesser {

	private CommandReceiver receiver;
	private int cmdLength;
	private byte[] welcomeStr;

	public ServerProcesser(CommandReceiver receiver, int cmdLength,
			byte[] welcomeStr) {
		this.receiver = receiver;
		this.cmdLength = cmdLength;
		this.welcomeStr = welcomeStr;
	}

	public void process(SelectionKey key) {
		SocketChannel channel = null;
		SelectionKey clientKey = null;
		try {
			channel = ((ServerSocketChannel) key.channel()).accept();
			clientKey = channel.configureBlocking(false).register(
					key.selector(),
					SelectionKey.OP_READ,
					new ClientProcesser(new ContentBuffer(new ChannelSubject(
							channel, welcomeStr), receiver, cmdLength)));
		} catch (NullPointerException ex) {
			return;
		} catch (IOException ex) {
			if (clientKey != null) {
				clientKey.cancel();
			}
			if (channel != null) {
				try {
					channel.close();
				} catch (IOException ex1) {
				}
			}
		}
	}

	private static class ChannelSubject extends Client {
		SocketChannel channel;

		public ChannelSubject(SocketChannel channel, byte[] welcomeStr) {
			this.channel = channel;
			this.returnResult(welcomeStr, 0, welcomeStr.length);
		}

		public void returnResult(byte[] result, int offset, int len) {
			try {
				this.channel.write(ByteBuffer.wrap(result, offset, len));
			} catch (IOException e) {
			}
		}

		public boolean isReturnable() {
			return this.channel.isOpen();
		}

	}
}
