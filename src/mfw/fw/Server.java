/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.fw;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.Set;

import mfw.buff.CommandReceiver;
import mfw.sel.CoreSelector;
import mfw.sel.SelectorInitTool;

public class Server {
	private byte[] welcomeStr = new byte[0];
	private CommandReceiver coreReceiver = null;
	private SocketAddress address = null;
	private int cmdLength = 128;

	public byte[] getWelcomeStr() {
		return welcomeStr;
	}

	public void setWelcomeStr(byte[] welcomeStr) {
		this.welcomeStr = welcomeStr;
	}

	public CommandReceiver getCoreReceiver() {
		return coreReceiver;
	}

	public void setCoreReceiver(CommandReceiver coreReceiver) {
		this.coreReceiver = coreReceiver;
	}

	public SocketAddress getAddress() {
		return address;
	}

	public void setAddress(SocketAddress address) {
		this.address = address;
	}

	public int getCmdLength() {
		return cmdLength;
	}

	public void setCmdLength(int cmdLength) {
		this.cmdLength = cmdLength;
	}

	private SelectorInitTool sit = new SelectorInitTool() {

		public void init(Selector sel) {
			ServerSocketChannel srvChannel;
			try {
				srvChannel = ServerSocketChannel.open();
				srvChannel.socket().bind(address);
				srvChannel.configureBlocking(false)
						.register(
								sel,
								SelectionKey.OP_ACCEPT,
								new ServerProcesser(coreReceiver, cmdLength,
										welcomeStr));
				keys = sel.keys();
			} catch (IOException e) {
				Server.this.stop();
				throw new Error(e);
			}
		}
	};

	private CoreSelector selector = null;
	private Set<SelectionKey> keys = null;

	public Set<SelectionKey> getKeys() {
		return keys;
	}

	public void start() {
		if (selector == null)
			selector = new CoreSelector(sit);
	}

	public void stop() {
		if (selector != null) {
			selector.close();
			selector = null;
			keys = null;
		}
	}
}
