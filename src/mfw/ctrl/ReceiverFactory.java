/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.ctrl;

import java.lang.reflect.InvocationTargetException;

import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import mfw.buff.CommandReceiver;
import mfw.buff.Returnable;
import mfw.pool.Mission;

public class ReceiverFactory {

	public static CommandReceiver newReceiverInstance(Node node)
			throws IllegalArgumentException, SecurityException, DOMException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			ClassNotFoundException {
		return new CommandReceiverImpl(parse(node));
	}

	public static Trigger parse(Node node) throws IllegalArgumentException,
			SecurityException, DOMException, InstantiationException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, ClassNotFoundException {
		if (node.getNodeName().equals("trigger")) {
			NamedNodeMap attributes = node.getAttributes();
			byte[] cmd = attributes.getNamedItem("cmd").getNodeValue()
					.getBytes();
			Trigger result = (Trigger) Class.forName(
					attributes.getNamedItem("class").getNodeValue())
					.newInstance();
			result._init(Cmd.create(cmd, 0, cmd.length));
			return result;
		} else if (node.getNodeName().equals("filter")) {
			byte[] cmd = node.getAttributes().getNamedItem("cmd")
					.getNodeValue().getBytes();
			NodeList list = node.getChildNodes();
			Trigger[] l = new Trigger[list.getLength()];
			int len = 0;
			for (int i = 0; i < list.getLength(); i++) {
				Trigger t = parse(list.item(i));
				if (t != null)
					l[len++] = t;
			}
			Trigger[] swap = new Trigger[len];
			System.arraycopy(l, 0, swap, 0, len);
			Filter result = new Filter();
			result._init(Cmd.create(cmd, 0, cmd.length), swap);
			return result;
		}
		return null;
	}

	private static class CommandReceiverImpl implements CommandReceiver {

		private Trigger root;

		public CommandReceiverImpl(Trigger root) {
			this.root = root;
		}

		public void receive(Returnable subject, byte[] content, int offset,
				int len) {
			Cmd.Parameter param = Cmd.create(content, offset, len);
			new MissionImpl(subject, param, root).start();
		}

	}

	private static class MissionImpl extends Mission {
		private Cmd.Parameter param;
		private Trigger root;
		private Returnable subject;

		public MissionImpl(Returnable subject, Cmd.Parameter param, Trigger root) {
			this.param = param;
			this.subject = subject;
			this.root = root;
		}

		public void run() {
			root.execute(subject, param);
		}
	}
}
