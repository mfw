/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.ctrl;

public class Cmd {

	public static Parameter create(byte[] core, int offset, int len) {
		byte[] b = new byte[len];
		System.arraycopy(core, offset, b, 0, len);
		return new Parameter(b, 0, b.length);
	}

	public static class Word {

		final byte[] core;
		final int offset;
		final int len;

		private Word(byte[] core, int offset, int len) {
			int end = offset + len;
			while (end > offset && core[end - 1] == ' ')
				end--;
			while (offset < end && core[offset] == ' ')
				offset++;
			this.core = core;
			this.offset = offset;
			this.len = end - offset;
		}

		public boolean isEquals(Word another) {
			if (this == another) {
				return true;
			}
			if (this.len == another.len) {
				int i = this.offset;
				int j = another.offset;
				int end = this.offset + this.len;
				while (i < end && this.core[i++] == another.core[j++])
					;
				if (i >= end)
					return true;
			}
			return false;
		}

		public byte[] getBytes() {
			byte[] result = new byte[len];
			System.arraycopy(core, offset, result, 0, len);
			return result;
		}
	}

	public static class Parameter extends Word {

		private Word verb = null;
		private Parameter param = null;

		private Parameter(byte[] core, int offset, int len) {
			super(core, offset, len);
		}

		public Word getVerb() {
			if (verb == null) {
				int i = offset;
				int end = offset + len;
				while (i < end && core[i] != ' ')
					i++;
				verb = new Word(core, offset, i - offset);
				param = new Parameter(core, i, offset + len - i);
			}
			return verb;
		}

		public Parameter getParameter() {
			if (param == null) {
				getVerb();
			}
			return param;
		}
	}
}
