/**
 *    Copyright 2011 Huize Dai
 *
 *    This file is part of MFW.
 *
 *    MFW is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MFW is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MFW.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfw.ctrl;

import mfw.buff.Returnable;

public class Filter extends Trigger {

	private Trigger[] list;

	protected Filter() {
	}

	void _init(Cmd.Word cmd, Trigger[] list) {
		super._init(cmd);
		this.list = list;
	}

	protected void execute(Returnable subject, Cmd.Parameter param) {
		Cmd.Word verb = param.getVerb();
		for (int i = 0; i < list.length; i++) {
			if (list[i].condition(verb)) {
				list[i].execute(subject, param.getParameter());
				return;
			}
		}
	}
}
